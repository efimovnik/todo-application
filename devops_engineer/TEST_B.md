# Devops Code Test: 'System Provisioning'

Welcome to the Devops code test! This will be a test of your:
1. Ability to work with Infrastructure as Code
2. Familiarity with automation practices
3. System control and management

The point of this test is to understand how you approach a technical problem. The way you code is just as important as your solution. 

*Please communicate with us via email as if you already work here, and add documentation/comments as you go along.*

We expect this test to take you about 4 - 8 hours. Given that most people are busy, We'd like to hear back from you within 72 hours of starting.

Send us an email when you're finished and we'll schedule a code review with engineers from across our teams to discuss your submission. We'll discuss the finer
 points of your implementation, walk through your code, demo its functionality (or troubleshoot its brokenness), and give feedback on its strengths, weaknesses 
 and, if we're being honest with ourselves, bugs. We don't expect your submission to be perfect, but we do expect you to be able to frankly communicate and 
 discuss your submission in a professional environment.

## Overall Requirements
Your task is to provide an automated method for provisioning a system (cloud, virtual machine, or hardware, your choice), which will have the `sl` package installed. This will resulting system will be a debian bullseye system, and you will need to make sure that the user `opendrives` can access the system, with sudo privileges, using the specified public key below. The system must also have a defined (not generated) hostname.

You may use any technologies that you like to achieve the final outcome. You may also use any language for scripting that you like, though we would recommend python/bash, (as long as it isn't [Ook!](https://esolangs.org/wiki/Ook!) ).

#### As soon as you begin:
* [ ] Fork this repository into a public repository on your gitlab/etc account

#### While working on this, please:
* [ ] Commit early and often. We'll likely be following along with your progress.

#### Upon completing this, please post to us:
* [ ] A link to your git repository such that we may view your code.
* [ ] A link to instructions so that we may test your solution.
* [ ] A Video of your solution in action

## Technical Requirements
The processes for this provisioning may be written in whatever language, and any technologies you choose, so long as it is operable after setting up the prerequisites (beware libraries that do not come installed by default). This process should assume that there is no existing system (unless you decide to provision a hardware system, of course).

### Automation
* Provisioning (Choose one)
    * [ ] A process to create a virtual system (cloud, vmware, virtualbox)
    * [ ] A process to power on a hardware device for installation via ISO/CD/PXE
* [ ] Debian Bullseye machine
* [ ] `sl` package installed 
* [ ] `opendrives` user with the ssh key specified below allowed
* [ ] `opendrives` user has sudo privileges (no password)
* [ ] SSH Key Only Access
* [ ] Hostname is not a generated hostname (EX: `mycodetest-opendrives.mycooldomain.com`)

### Scripting
* [ ] Ingestion of hostname, user, ssh key for provisioning
* [ ] Supply a list of additional packages to install via apt during provision

### Reproducibility
* [ ] Should be able to recreate your process, EX: Infrastructure as Code, or a set of scripts to stand up your solution.

### Documentation
* [ ] Should be able to gain a good understanding and overview of your solution, technologies used, and workflow from some form of documentation.
* [ ] Documentation of how to setup and run your solution

## SSH Key
`ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCfZ+3Ib7BTOnwnvhgg2s5Y7wE5yd81J+CT6JHAM2Bg4F6I9LPQnCY0q6XdDNI8T30SgQMTp/8V9T5dfSMm5YG+rLmn/maIMLjJl9Obr4512g/6kUShkS7Oa3ucUVeTsa8PRjdql2BU8Wv3CNm5rCV0d7zixOPUXxwfx5fHmJeyDfRlh+trJ4mtB449xommLEklRXW2wyWvwr3ITc9u4uYlMiSVLuAP4wiaAAaNwLotnkS03pHvaBpQ6pH0nrUmi4vnoXdIxrHGqm/VUT9RND0dlTt1XShZ8Yuw9DxEbWNHVVnfMjiMso9ZOrfreZomfTC/knCWD34O2V5Rl8JzOcM7 code@opendrives.com`

## BONUS

If you're so cool that even the cool cats step back, and knocked out the above already, consider adding some improvements to what you have made.

### Suggestions for improvements
* [ ] A 'One Button' Solution, with user input/prompts, or command line arguments
* [ ] Solutions for multiple system types (EX: 2 cloud providers, or a virtual and cloud, etc)
* [ ] Multiple Users, or Multiple Keys
* [ ] Deploy other OSes, with the same requirements. Any OS you want, but must be able to be demonstrated during review.
* [ ] Edit this readme with new suggestions on how to improve this code test